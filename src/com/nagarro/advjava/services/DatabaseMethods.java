package com.nagarro.advjava.services;

import java.io.File;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.util.MarkerObject;

import com.nagarro.advjava.services.hibernate.Image;

public class DatabaseMethods {
	public static void saveImage(File file, String src) {
		
		System.out.println("File name :"+file.getAbsolutePath()+" "+file.getName() + file.length());
			
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Image.class)
				.buildSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		try {
		
					
			Image u = new Image(file.getName(), file.getAbsolutePath());
					
			session.save(u);
					
			session.getTransaction().commit();
			
			//
					
		} finally {
//			session.flush();
//			session.close();
//			factory.close();
		}		
	}	
	
	public static List<Image> getImages() {
		
		List <Image> list = null;
		//System.out.println("File name :"+file.getAbsolutePath()+" "+file.getName() + file.length());
			
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Image.class)
				.buildSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		try {
		
					
		Query query = session.createQuery("from Image");
        
		list = query.list();
        
        System.out.println(list);
        
        session.getTransaction().commit();
        
					
		} finally {
//			session.flush();
			//session.close();
			//factory.close();
		}	
		return list;
	}
	
	public static String delImage(String name)
	{
	    Session session ;
	    Image img ;
	    String str = null;
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Image.class)
				.buildSessionFactory();
		
		session = factory.getCurrentSession();
		session.beginTransaction();
		
		try {
			
			
			Query query = session.createQuery("delete Image where img_name = ?").setString(0, name);
			
			int result = query.executeUpdate();
			 
			if (result > 0) {
			   str = "Item Deleted successfully";
			} else {
				str = "Opps !!! there was some issue";
			}

		    //This makes the pending delete to be done
			session.getTransaction().commit();
		} finally {
			// TODO: handle finally clause
			//session.flush();
			//session.close();
			//factory.close();
		}
	    
		return str;
	}
}
