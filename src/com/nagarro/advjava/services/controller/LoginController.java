package com.nagarro.advjava.services.controller;

import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nagarro.advjava.services.LoginService;
import com.nagarro.advjava.services.LoginServiceImp;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/login")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("login.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");  
		  
		System.out.println( request +username + " :: " + password);
		String page = "index.jsp";
		if(username.trim().length() >= 0 && username != null &&
				password.trim().length() >= 0 && password != null) {
		  
			LoginService loginService = new LoginServiceImp();		   
			boolean flag = loginService.isUserValid(username, password);
		   
		   
		   if(flag) {
				System.out.println("Login success!!!");
				request.setAttribute("username", username);
				request.setAttribute("msg", "Login Success.....");
				page = "imagegallery.jsp";
		   } else {
			   request.setAttribute("msg", "Wrong Username or Password, Try again!!!");
		   }
		 
		   request.setAttribute("msg", "Please enter username and password...");
			}
		  //response.sendRedirect(request.getContextPath() + "/"+page);
		  request.getRequestDispatcher(page).include(request, response);
		 }
	}

