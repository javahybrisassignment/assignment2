package com.nagarro.advjava.services.hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.nagarro.advjava.services.Utility;

@Entity
@Table(name="images")
public class Image {

	@Id
	@Column(name="img_name")
	private String imgName;
	
	
	@Column(name="img_src")
	private String imgSrc;
	
	public Image() {
		
	}
	
	public Image(String imgName, String imgSrc) {
		this.imgName = imgName;
		this.imgSrc = imgSrc;
	}

	public String getImgName() {
		return imgName;
	}

	public void setImgName(String imgName) {
		this.imgName = imgName;
	}

	public String getImgSrc() {
		return imgSrc;
	}

	public void setImgSrc(String imgSrc) {
		this.imgSrc = imgSrc;
	}

	public String getImgSize(String imgSrc) {
		float size = (float) 0.0;
		size = Utility.calFileSize(this.imgSrc) ;
		return size+" Kb";
	}
	
	
}
