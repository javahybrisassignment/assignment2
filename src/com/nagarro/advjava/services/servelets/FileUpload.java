package com.nagarro.advjava.services.servelets;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.io.FileUtils;

import com.nagarro.advjava.services.DatabaseMethods;
import com.nagarro.advjava.services.Utility;
import com.nagarro.advjava.services.hibernate.Image;

/**
 * Servlet implementation class FileUpload
 */
@WebServlet("/upload")
@MultipartConfig
public class FileUpload extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FileUpload() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String description = request.getParameter("description"); // Retrieves <input type="text" name="description">
	    Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">
	    String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
	    //InputStream fileContent = filePart.getInputStream();
	    String uploadDir = "D:\\sm_wd\\java_Projects\\assignment2\\src\\main\\webapp\\images";
	    
	    List<Image> list = DatabaseMethods.getImages();
	    long totalFileSize = 0;
	    long fileSize = filePart.getSize()/(1024);
	    for(Image i : list) {
	    	totalFileSize += Utility.calFileSize(i.getImgSrc());
	    }
	    
	    totalFileSize += fileSize;
	    
	    if((fileSize/1024) <= 1 && totalFileSize<=(10*1024)) {
	    	System.out.println("file name : "+ fileName);
		    // ... (do your job here)
		    File file = new File(uploadDir, fileName);
		    
		    try (InputStream fileContent = filePart.getInputStream()) {
		    	FileUtils.copyInputStreamToFile(fileContent, file);
		    }
		    
		    DatabaseMethods.saveImage(file, fileName);
	    } else {
	    	String msg = (fileSize/1024) <= 1 ? "File cannot be uploaded server caapacity reached":"File should be smaller than 1 mb";
	    	System.out.println("msg : "+msg);
	    }	    
    }

}
