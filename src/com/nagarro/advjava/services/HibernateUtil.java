package com.nagarro.advjava.services;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.nagarro.advjava.services.hibernate.Image;
import com.nagarro.advjava.services.hibernate.User;

public class HibernateUtil {
	
	
	 private static Configuration configuration;
	 private static SessionFactory factory;

	 static {
		 factory = new Configuration()
					.configure("hibernate.cfg.xml")
					.addAnnotatedClass(Image.class)
					.addAnnotatedClass(User.class)
					.buildSessionFactory();
		
	 }
	 
	 public static Session getSession() {
	  Session session = null;
	  if (factory != null) {
	   session = factory.openSession();
	  }
	  return session;
	 }	
}
