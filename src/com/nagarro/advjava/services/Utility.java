package com.nagarro.advjava.services;

import java.io.File;

public class Utility {
	
	public static float calFileSize(String src) {
		float size;
		File f = new File(src);
		if(f.exists()) {
			size = f.length()/(1024);
		}else {
			size = (float) 0.0;
		}
	
		return size;
	}
	
	
}
