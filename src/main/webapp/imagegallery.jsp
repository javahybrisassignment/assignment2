<%@page import="com.nagarro.advjava.services.hibernate.Image"%>
<%@page import="java.util.List"%>
<%@page import="com.nagarro.advjava.services.DatabaseMethods"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Image Gallery</title>
		<script  src="https://code.jquery.com/jquery-3.4.1.min.js"  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="  crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="css/imggallery.css">
	
		<script type="text/javascript">
			$('body').on('click','#my-btn',function(){
				alert('it works');
			});
			
			
			$(document).on('click', '.opt-del',function(e){
				alert('Do you want to delete this Image');
				console.log("e : ");
				console.log(e.target.name);
								
				$.get("deleteimage",
					{
                     imgName : e.target.name
	                },
	           	function(res) {  
	                location.reload();	
			    	console.log(res)   
			    });
			});
			
			
			/* $(document).on('click', '#uploadSubmit',function(e){
			/*	alert('Upload Image');
				console.log("e : ");
				console.log(e.target.name);				
				$.post("uploadimage",
	           	function(res) {  
	                location.reload();	
			    	console.log(res)   
			    }); 
			    
			    var data = new FormData();
			    $.each(jQuery('#customFile')[0].files, function(i, file) {
			        data.append('file-'+i, file);
			    });
			    
			    $.ajax({
			        url: 'uploadimage',
			        data: data,
			        cache: false,
			        contentType: false,
			        processData: false,
			        method: 'POST',
			        type: 'POST', // For jQuery < 1.9
			        success: function(data){
			            alert(data);
			        }
			    });
			    
				event.preventDefault();*/
				
				
				$("#uploadSubmit").click(function (event) {

			       //stop submit the form, we will post it manually.
			       event.preventDefault();
			
			       // Get form
			       var form = $('#fileform')[0];
			
				// Create an FormData object 
			       var data = new FormData(form);
			
				// If you want to add an extra field for the FormData
			       //data.append("CustomField", "This is some extra data, testing");
			
				// disabled the submit button
			       $("#uploadSubmit").prop("disabled", true);
			
			       $.ajax({
			           type: "POST",
			           enctype: 'multipart/form-data',
			           url: "upload",
			           data: data,
			           processData: false,
			           contentType: false,
			           cache: false,
			           timeout: 600000,
			           success: function (data) {
			
			               $("#result").text(data);
			               console.log("SUCCESS : ", data);
			               $("#btnSubmit").prop("disabled", false);
			
			           },
			           error: function (e) {
			
			               $("#result").text(e.responseText);
			               console.log("ERROR : ", e);
			               $("#uploadSubmit").prop("disabled", false);
			
			           }
			       });
			       
			       event.preventDefault();
			
			   });

		</script>
	
	
	
	</head>
	<body>
	
		<div class="jumbotron txtcenter">
		  <h1 class="display-4">Image Management Utility</h1>
		</div>
		
		<div class="col-md-8">
			<form id="fileform" action="upload" method="post" enctype="multipart/form-data">  			
	  			<label class="my-1 mr-2" for="file-form">Please select an image file to upload (Max Size 1 MB)</label><br>
	  			<div class="form-row align-items-center">
					<div class="custom-file col-md-4" id="file-form">
		  				<input type="file" class="custom-file-input" id="customFile" name="file">
		  				<label class="custom-file-label" for="customFile">Choose file</label>
					</div>
					<div class="col-auto my-2 float-right">
	      				<button type="submit" id="uploadSubmit" class="btn btn-primary">Upload</button>
	      				<button type="reset" class="btn btn-primary" id="my-btn">Cancel</button>
	   				</div>
	   				<div><h5 class="center" id="result"></h5></div>
	 			</div>
	 			<script>
	 			$('.custom-file-input').on('change', function() { 
	 				console.log("filename");
	 			    let fileName = $(this).val().split('\\').pop(); 
	 			    $(this).next('.custom-file-label').html(fileName);
	 			    
	 			   if($(this).get(0).files.length > 0){ // only if a file is selected
					    var fileSize = $(this).get(0).files[0].size;
					    if(fileSize>1000000){
					    	$(this).val = "";
					    	alert("File size is greater than 1 MB");
					    } 	
					  }
	 			});
		        </script>
 			</form>			
		</div>
		
		<hr><br>
		<% 
			List<Image> l = DatabaseMethods.getImages();
			int size = l.size();
		%>
		
		<div class="col-md-11 text-center">
			<h2>Uploaded Images</h2>
				<div class="img-gallery">
					<table class="table">
					  <thead class="thead-light">
					    <tr>
					      <th scope="col">Sl. No.</th>
					      <th scope="col">Name</th>
					      <th scope="col">Size</th>
					      <th scope="col">Preview</th>
					      <th scope="col">Actions</th>
					    </tr>
					  </thead>
					  <tbody>
						<% for(int i = 0; i < size; i++) {
							Image img = l.get(i);
						%>
			            <tr>      
			                <th><%=i+1 %></th>
			                <td><%=img.getImgName() %></td>
			                <td><%=img.getImgSize(img.getImgSrc()) %></td>
			                <td>
			                	<img src="<%="images/"+img.getImgName() %>" width="200" height="200" alt="<%=img.getImgName() %>" class="img-thumbnail">			                
			                </td>
			                <td>
			                	<img src="images/tools.svg" class="opt-edit" alt="" width="32" height="32" title="Bootstrap">
					      		<img src="images/trash.svg" class="opt-del" name="<%=img.getImgName()%>" alt="" width="32" height="32" title="Bootstrap">
					      	</td>  
			            </tr>
					    <% } %>		    
					    
					  </tbody>
					</table>
				</div>
		</div>
		
	
	</body>
</html>

